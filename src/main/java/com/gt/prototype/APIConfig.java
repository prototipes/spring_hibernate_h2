package com.gt.prototype;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.sql.SQLException;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.jmx.ConnectionPool;

@Configuration
@EnableJpaRepositories("com.gt.prototype")
@EnableTransactionManagement
@EntityScan
@EnableCaching
@PropertySource(ignoreResourceNotFound = false, value = "classpath:application.properties")
@ComponentScan(basePackageClasses = { APIConfig.class })
public class APIConfig {

	@Bean
	@ConditionalOnExpression("${spring.datasource.jmxEnabled:true}")
	public ConnectionPool jdbcPool(DataSource dataSource) throws SQLException {
		return dataSource.createPool().getJmxPool();
	}

}