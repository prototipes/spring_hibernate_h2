package com.gt.prototype.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name = "product")
public class Product implements Serializable{

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "PROD_ID", unique = true, nullable = false)
	private Integer prodId;

	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "DESCRIPTION", nullable = false)
	private String description;

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="PARENT")
	private Product parent;

	@OneToMany(mappedBy="parent")
	private Set<Product> childs = new HashSet<Product>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
	private Set<Image> images = new HashSet<Image>(0);
	
	public Product() {
	}
	
	public Product(String name, String description, Product parentProduct) {
		this.name = name;
		this.description = description;
		this.parent = parentProduct;
	}

	public Integer getProdId() {
		return prodId;
	}

	public void setProdId(Integer id) {
		this.prodId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}

	public Set<Product> getChilds() {
		return childs;
	}

	public void setChilds(Set<Product> childs) {
		this.childs = childs;
	}

	public Set<Image> getImages() {
		return this.images;
	}

	public void setImages(Set<Image> images) {
		this.images = images;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}	
}
