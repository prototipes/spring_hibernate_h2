package com.gt.prototype.service.impl;

import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gt.prototype.entity.Image;
import com.gt.prototype.entity.Product;
import com.gt.prototype.repository.ImageRepository;
import com.gt.prototype.repository.ProductRepository;
import com.gt.prototype.service.ProductService;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ImageRepository imageRepository;

	@Override
	@Cacheable("products")
	public List<Product> listProducts() {
		LOGGER.debug("listProducts()");
		List<Product> productEntities = (List<Product>) productRepository.findAll();
		return productEntities;
	}

	@Override
	@Cacheable(cacheNames = "product", key = "#productId")
	public Product getProduct(Integer productId) {
		LOGGER.debug("getChildProduct({})", productId);
		return productRepository.findOne(productId);
	}

	@Override
	@Caching(evict = { @CacheEvict(cacheNames = "products",allEntries=true), 
			@CacheEvict(cacheNames = "product", key = "#productId"),
			@CacheEvict(cacheNames = "childImg", key = "#productId") })
	public Product addImage(Integer productId) {
		LOGGER.debug("getChildProduct({})", productId);
		Product prod = productRepository.findOne(productId);
		Image newImage = new Image(prod);
		imageRepository.save(newImage);

		prod.getImages().add(newImage);

		return prod;
	}

	@Override
	@Cacheable(cacheNames = "childProd", key = "#productId")
	public List<Product> getChildProduct(Integer productId) {
		LOGGER.debug("getChildProduct({})", productId);
		List<Product> products = null;
		Product prod = productRepository.findOne(productId);
		if (prod != null && prod.getChilds().size() > 0) {
			products = new LinkedList<Product>();
			products.addAll(prod.getChilds());
		}
		return products;
	}

	@Override
	@Cacheable(cacheNames = "childImg", key = "#productId")
	public List<Image> getImages(Integer productId) {
		LOGGER.debug("getImages({})", productId);
		List<Image> images = null;
		Product prod = productRepository.findOne(productId);
		if (prod != null && prod.getImages().size() > 0) {
			images = new LinkedList<Image>();
			images.addAll(prod.getImages());
		}
		return images;
	}
}