package com.gt.prototype.service;

import java.util.List;
import java.util.Set;

import com.gt.prototype.entity.Image;
import com.gt.prototype.entity.Product;

public interface ProductService {

	List<Product> listProducts();

	List<Product> getChildProduct(Integer product);

	Product getProduct(Integer product);

	Product addImage(Integer product);

	List<Image> getImages(Integer product);

}
