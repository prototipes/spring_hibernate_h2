package com.gt.prototype.api.response;

public class ImageArrayResponse {

	private ImageResponse[] images;

	public ImageArrayResponse() {
	}

	public ImageArrayResponse(ImageResponse[] images) {
		this.images = images;
	}

	public ImageResponse[] getImages() {
		return images;
	}

	public void setImages(ImageResponse[] images) {
		this.images = images;
	}	

}