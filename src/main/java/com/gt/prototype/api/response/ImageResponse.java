package com.gt.prototype.api.response;

public class ImageResponse {

	private Integer id;
	
	public ImageResponse() {
	}

	public ImageResponse(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}