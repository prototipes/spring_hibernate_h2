package com.gt.prototype.api.response;


public class ProductResponse {

	private Integer id;
	private String name;
	private String description;
	private ProductResponse parent;
	private ImageResponse[] images;

	public ProductResponse() {
	}

	public ProductResponse(Integer id, String name, String description, ProductResponse parent,
			ImageResponse[] images) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.parent = parent;
		this.images = images;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ProductResponse getParent() {
		return parent;
	}

	public void setParent(ProductResponse parent) {
		this.parent = parent;
	}

	public ImageResponse[] getImages() {
		return images;
	}

	public void setImages(ImageResponse[] images) {
		this.images = images;
	}
	
	
	
}