package com.gt.prototype.api.response;

public class ProductArrayResponse {

	private ProductResponse[] products;

    public ProductArrayResponse() {
	}
    
	public ProductArrayResponse(ProductResponse[] products) {
		this.products = products;
	}

	public ProductResponse[] getProducts() {
		return products;
	}

	public void setProducts(ProductResponse[] products) {
		this.products = products;
	}
}