package com.gt.prototype.api;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gt.prototype.api.response.ImageArrayResponse;
import com.gt.prototype.api.response.ImageResponse;
import com.gt.prototype.api.response.ProductArrayResponse;
import com.gt.prototype.api.response.ProductResponse;
import com.gt.prototype.entity.Image;
import com.gt.prototype.entity.Product;
import com.gt.prototype.service.ProductService;

@RestController
@RequestMapping
public class Resource {

	public static final String PRODUCTS = "/products";
	public static final String PRODUCT_BY_PRODUCTID = "/product/{id}";
	public static final String PRODUCT_IMAGES_BY_PRODUCTID = "/product/{id}/images";
	public static final String PRODUCT_ADDIMAGES_TO_PRODUCTID = "/product/{id}/images/add";
	public static final String PRODUCT_CHILDS_BY_PRODUCTID = "/product/{id}/childs";
	public static final String COMPLETE_PRODUCTS = "/completeproducts";

	private static final Logger LOGGER = LoggerFactory.getLogger(Resource.class);

	@Autowired
	private ProductService productService;

	@GetMapping(path = PRODUCTS, produces = MediaType.APPLICATION_JSON_VALUE)
	public ProductArrayResponse getProducts() throws Exception {

		LOGGER.info("getProducts()");

		try {
			List<Product> productEntities = productService.listProducts();

			return fromEntityListToResponse(productEntities);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@GetMapping(path = PRODUCT_BY_PRODUCTID, produces = MediaType.APPLICATION_JSON_VALUE)
	public ProductResponse getProductById(@PathVariable("id")Integer prodId) throws Exception {

		LOGGER.info("getProductById({})",prodId);

		try {
			Product product = productService.getProduct(prodId);

			return fromEntityToResponse(product);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@GetMapping(path = PRODUCT_CHILDS_BY_PRODUCTID, produces = MediaType.APPLICATION_JSON_VALUE)
	public ProductArrayResponse getProductChildById(@PathVariable("id")Integer prodId) throws Exception {

		LOGGER.info("getProductChildById({})",prodId);

		try {
			List<Product> productEntities = productService.getChildProduct(prodId);

			return fromEntityListToResponse(productEntities);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	@GetMapping(path = PRODUCT_ADDIMAGES_TO_PRODUCTID, produces = MediaType.APPLICATION_JSON_VALUE)
	public ProductResponse addImagesToProductById(@PathVariable("id")Integer prodId) throws Exception {

		LOGGER.info("getProductImagesById({})",prodId);

		try {
			Product product = productService.addImage(prodId);


			return fromEntityToResponse(product);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	@GetMapping(path = PRODUCT_IMAGES_BY_PRODUCTID, produces = MediaType.APPLICATION_JSON_VALUE)
	public ImageArrayResponse getProductImagesById(@PathVariable("id")Integer prodId) throws Exception {

		LOGGER.info("getProductImagesById({})",prodId);

		try {
			List<Image> images = productService.getImages(prodId);


			return new ImageArrayResponse(fromImageEntityListToResponse(images));
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	private ProductArrayResponse fromEntityListToResponse(List<Product> productEntities) {
		ProductArrayResponse responseArray = null;
		if (productEntities != null && productEntities.size() > 0) {
			ProductResponse[] response = new ProductResponse[productEntities.size()];
			int i = 0;
			for (Product productEntity : productEntities) {
				response[i++] = fromEntityToResponse(productEntity);
			}
			responseArray = new ProductArrayResponse(response);
		}
		return responseArray;
	}

	private ImageResponse[] fromImageEntityListToResponse(List<Image> imageEntities) {
		ImageResponse[] response = null;
		if (imageEntities != null && imageEntities.size() > 0) {
			response = new ImageResponse[imageEntities.size()];
			int i = 0;
			for (Image imageEntity : imageEntities) {
				response[i++] = fromEntityToResponse(imageEntity);
			}
		}
		return response;
	}

	private ProductResponse fromEntityToResponse(Product product) {
		ProductResponse result = null;
		if (product != null) {			
			List images = new LinkedList<Image>();
			images.addAll(product.getImages());
			result = new ProductResponse(product.getProdId(), product.getName(), product.getDescription(),
					fromEntityToResponse(product.getParent()), fromImageEntityListToResponse(images));
		}
		return result;
	}

	private ImageResponse fromEntityToResponse(Image image) {
		ImageResponse result = null;
		if (image != null) {
			result = new ImageResponse(image.getId());
		}
		return result;
	}
}
