package com.gt.prototype.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.gt.prototype.APIConfig;

@SpringBootApplication
@Import({APIConfig.class})
public class APIStarter {

    public static void main(String[] args) {
        SpringApplication.run(APIStarter.class, args);
        
    }

}
