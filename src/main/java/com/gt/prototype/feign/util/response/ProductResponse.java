package com.gt.prototype.feign.util.response;

import com.gt.prototype.api.response.ImageResponse;

public class ProductResponse extends com.gt.prototype.api.response.ProductResponse {
	private boolean fallback;

	public ProductResponse() {
	}

	public ProductResponse(Integer id, String name, String description, ProductResponse parent,
			ImageResponse[] images) {
		super(id, name, description, parent, images);
	}

	public boolean isFallback() {
		return fallback;
	}

	public void setFallback(boolean fallback) {
		this.fallback = fallback;
	}
}
