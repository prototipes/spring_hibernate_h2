package com.gt.prototype.feign.util.response;

import com.gt.prototype.api.response.ProductResponse;

public class ProductArrayResponse extends com.gt.prototype.api.response.ProductArrayResponse {
	
	private boolean fallback;

	public ProductArrayResponse() {
		super();
	}

	public ProductArrayResponse(ProductResponse[] products) {
		super(products);
	}
	
	public boolean isFallback() {
		return fallback;
	}

	public void setFallback(boolean fallback) {
		this.fallback = fallback;
	}
	
	

}
