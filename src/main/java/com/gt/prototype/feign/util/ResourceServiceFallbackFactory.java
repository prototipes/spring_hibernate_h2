/*package com.gt.prototype.feign.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.gt.prototype.feign.util.response.ProductArrayResponse;

import feign.hystrix.FallbackFactory;

@Component
public class ResourceServiceFallbackFactory implements
        FallbackFactory<ResourceService> {

    @Override
    public ResourceService create(Throwable cause) {
        return new ResourceServiceErrorFallback(cause);
    }

    private class ResourceServiceErrorFallback implements
    ResourceService {

        private final Throwable cause;

        private final Logger LOGGER = LoggerFactory.getLogger
                (ResourceServiceErrorFallback.class);

        private ResourceServiceErrorFallback(Throwable cause) {
            this.cause = cause;
        }

		@Override
		public ProductArrayResponse getAllProducts() {
            LOGGER.error("m=getTransaction, cause={}", cause);
            ProductArrayResponse response = new ProductArrayResponse();
            response.setFallback(true);
            return response;
		}
    }

}*/
