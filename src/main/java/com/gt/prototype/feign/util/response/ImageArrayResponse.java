package com.gt.prototype.feign.util.response;

import com.gt.prototype.api.response.ImageResponse;

public class ImageArrayResponse extends com.gt.prototype.api.response.ImageArrayResponse {
	private boolean fallback;

	public ImageArrayResponse() {
	}

	public ImageArrayResponse(ImageResponse[] images) {
		super(images);
	}
	
	public boolean isFallback() {
		return fallback;
	}

	public void setFallback(boolean fallback) {
		this.fallback = fallback;
	}
}
