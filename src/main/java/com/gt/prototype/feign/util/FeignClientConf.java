/*package com.gt.prototype.feign.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.lang.reflect.Type;

import feign.FeignException;
import feign.Logger;
import feign.Response;
import feign.codec.DecodeException;
import feign.codec.Decoder;

@Configuration
public class FeignClientConf implements BeanPostProcessor {

  @Override
  public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
    if (bean instanceof Decoder) {
      return new DecoderV2((Decoder) bean);
    }
    return bean;
  }

  @Override
  public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    return bean;
  }

  @Bean
  public Logger.Level logLevel(){
    return Logger.Level.FULL;
  }

  static class DecoderV2 implements Decoder {

    private final Decoder delegateTo;

    public DecoderV2(Decoder delegateTo) {
      this.delegateTo = delegateTo;
    }

    @Override
    public Object decode(Response response, Type type) throws IOException, DecodeException, FeignException {
      if (response.status() == HttpStatus.NOT_FOUND.value()) {
        return null;
      }
      return delegateTo.decode(response, type);
    }

  }
}*/