/*package com.gt.prototype.feign.util;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gt.prototype.api.response.ProductArrayResponse;

@FeignClient(name = "ResourceClient", 
url = "http://localhost:8080",
fallbackFactory = ResourceServiceFallbackFactory.class, 
configuration = FeignClientConf.class,
decode404 = true)
public interface ResourceService {

    @RequestMapping(value = "/products",
            method = RequestMethod.GET, 
            produces = MediaType.APPLICATION_JSON_VALUE)
    ProductArrayResponse getAllProducts();

}
*/