package com.gt.prototype.repository;

import org.springframework.data.repository.CrudRepository;

import com.gt.prototype.entity.Image;

public interface ImageRepository extends CrudRepository<Image, Integer> {
}
