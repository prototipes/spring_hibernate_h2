package com.gt.prototype.repository;

import org.springframework.data.repository.CrudRepository;

import com.gt.prototype.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {
}
