insert into PRODUCT(PROD_ID, NAME, DESCRIPTION, PARENT) values (1,'nome 1','desc 1', null);
insert into PRODUCT(PROD_ID, NAME, DESCRIPTION, PARENT) values (2,'nome 2','desc 2', 1);
insert into PRODUCT(PROD_ID, NAME, DESCRIPTION, PARENT) values (3,'nome 3','desc 3', 1);
insert into PRODUCT(PROD_ID, NAME, DESCRIPTION, PARENT) values (4,'nome 4','desc 4', null);
insert into PRODUCT(PROD_ID, NAME, DESCRIPTION, PARENT) values (5,'nome 5','desc 5', 4);

insert into IMAGE(IMAGE_ID, PROD_ID) values (1,1);
insert into IMAGE(IMAGE_ID, PROD_ID) values (2,1);
insert into IMAGE(IMAGE_ID, PROD_ID) values (3,1);
insert into IMAGE(IMAGE_ID, PROD_ID) values (4,2);
insert into IMAGE(IMAGE_ID, PROD_ID) values (5,2);
insert into IMAGE(IMAGE_ID, PROD_ID) values (6,3);
insert into IMAGE(IMAGE_ID, PROD_ID) values (7,3);
insert into IMAGE(IMAGE_ID, PROD_ID) values (8,4);
insert into IMAGE(IMAGE_ID, PROD_ID) values (9,5);
insert into IMAGE(IMAGE_ID, PROD_ID) values (10,5);
insert into IMAGE(IMAGE_ID, PROD_ID) values (11,5);