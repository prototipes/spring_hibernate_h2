package com.gt.prototype.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.gt.prototype.api.response.ImageArrayResponse;
import com.gt.prototype.api.response.ProductArrayResponse;
import com.gt.prototype.api.response.ProductResponse;

import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ResourceTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ResourceTest.class);
	
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testGetProducts() throws Exception {
		LOGGER.info("RUNINING testGetProducts()");
		ProductArrayResponse response = this.restTemplate.getForObject("http://localhost:" + port + "/products", ProductArrayResponse.class);
		assertTrue(response.getProducts().length == 5);
		LOGGER.info("testGetProducts() END");
		
	}

	@Test
	public void testGetProduct() throws Exception {
		LOGGER.info("RUNINING testGetProduct()");
		ProductResponse response = this.restTemplate.getForObject("http://localhost:" + port + "/product/1", ProductResponse.class);
		
		assertTrue(response.getId().equals(1));
		assertTrue(response.getName().equals("nome 1"));
		assertTrue(response.getDescription().equals("desc 1"));		

		LOGGER.info("testGetProduct() END");
	}

	@Test
	public void testGetChilds() throws Exception {
		LOGGER.info("RUNINING testGetProduct()");
		ProductArrayResponse response = this.restTemplate.getForObject("http://localhost:" + port + "/product/1/childs", ProductArrayResponse.class);
		
		assertTrue(response.getProducts().length == 2);

		LOGGER.info("testGetProduct() END");
	}

	@Test
	public void testGetImages() throws Exception {
		LOGGER.info("RUNINING testGetProduct()");
		ImageArrayResponse response = this.restTemplate.getForObject("http://localhost:" + port + "/product/2/images", ImageArrayResponse.class);

		assertTrue(response.getImages().length==2);

		LOGGER.info("testGetProduct() END");
	}

	@Test
	public void testAddImages() throws Exception {
		LOGGER.info("RUNINING testGetProduct()");
		ProductResponse response = this.restTemplate.getForObject("http://localhost:" + port + "/product/4/images/add", ProductResponse.class);

		assertTrue(response.getImages().length==2);

		LOGGER.info("testGetProduct() END");
	}
	
	
	
	
}
