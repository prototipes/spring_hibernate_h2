# SPRING_HIBERNATE_H2 
---
this app is just a prototype of use about SPRING SUITE FOR REST, HIBERNATE and IN-MEMORY DB.

### Requirement
---
Java 8+
mvn 3+

### to Compile
---
in the root of project
> mvn clean compile

### to run the app
---
in the root of project
> mvn spring-boot:run

### to run tests
---
in the root of project
> mvn test

### APIs
---
http://localhost:8080/products
Returns a list of all products with related child products and related images

http://localhost:8080/product/<ID>
Returns a simgle product with related child products and related images

http://localhost:8080/product/<ID>/childs
Returns a list of childs products of a specified product

http://localhost:8080/product/<ID>/images
Returns a list of related images of a specified product

http://localhost:8080/product/<ID>/images/add
Add a image to specified product

### H2 Database
---
This app have a in memory H2, to change the initials db entities: change the script in /src/main/resources/data-h2.sql, the db configuration details are in /src/main/resources/application.properties